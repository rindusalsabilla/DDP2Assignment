//package javari;

import javari.animal.Animal;
import javari.park.Attractions;
import javari.park.Registration;
import javari.park.SelectedAttraction;
import javari.park.Visitors;
import javari.reader.AnimalAttraction;
import javari.reader.AnimalCategories;
import javari.reader.CsvReader;
import javari.reader.AnimalRecords;
import javari.writer.RegistrationWriter;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class Controller {

    static Scanner inp = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.print("... Opening default section database from data. ");

        CsvReader[] files = new CsvReader[3];
        String textPath = System.getProperty("user.dir") + "\\src\\main\\java\\javari\\input";

        while (true) {
            try {
                files[0] = new AnimalCategories(Paths.get(textPath, "animals_categories.csv"));
                files[1] = new AnimalAttraction(Paths.get(textPath, "animals_attractions.csv"));
                files[2] = new AnimalRecords(Paths.get(textPath, "animals_records.csv"));
                System.out.println("\n... Loading... Success... System is populating data...\n");
                break;
            }

            catch (IOException e) {
                System.out.println("... File not found or incorrect file!\n");
                System.out.print("Please provide the source data path: ");
                textPath = inp.nextLine();
                textPath = textPath.replace("\\", "\\\\");
            }
        }

        System.out.printf("Found %d valid sections and %d invalid sections%n",
                files[0].countValidRecords(), files[0].countInvalidRecords());
        System.out.printf("Found %d valid attractions and %d invalid attractions%n",
                files[1].countValidRecords(), files[1].countInvalidRecords());
        System.out.printf("Found %d valid animal categories and %d invalid animal categories%n",
                files[0].countValidRecords(), files[0].countInvalidRecords());
        System.out.printf("Found %d valid animal records and %d invalid animal records%n",
                files[2].countValidRecords(), files[2].countInvalidRecords());

        Regist();
    }

    public static void Regist() {

        System.out.println("Welcome to Javari Park Festival - Registration Service!\n");
        System.out.println("Please answer the questions by typing the number. Type # if you want to return to the previous menu");

        if (CheckStatus()) System.out.println("Bye Fren");

    }

    public static boolean CheckStatus() {
        String input;

        while (true) {
            Set<String> x = AnimalCategories.getSection().keySet();
            ArrayList<String> temp = new ArrayList<>();
            System.out.println("\nJavari Park has " + x.size() + " sections:");

            int i = 0;
            for (String section : x) {
                System.out.println(++i + ". " + section);
                temp.add(section);
            }

            System.out.print("Please choose your preferred section (type the number): ");

            input = inp.nextLine();
            if (input.equals("#")) return true;

            try {
                int inp = Integer.parseInt(input);
                if (inp > 0 && inp <= i)
                    if (!animalCheck(temp.get(inp - 1))) return false;
            }

            catch (NumberFormatException e) {
                System.out.println("You do nothing...");
            }
        }
    }

    public static boolean animalCheck(String sesi) {
        String input;

        while (true) {
            System.out.println("\n--" + sesi + "--");
            int i = 0;
            List<String> temp = CsvReader.getAnimalMap().get(sesi);
            for (String hewan : temp) {
                System.out.println(++i + ". " + hewan);
            }

            System.out.print("Please choose your preferred animals (type the number): ");

            input = inp.nextLine();
            if (input.equals("#")) return true;

            try {
                int inp = Integer.parseInt(input);
                if (inp > 0 && inp <= i)
                    if (!attractionCheck(temp.get(inp - 1)))
                        return false;
            }

            catch (NumberFormatException e) {
                System.out.println("You do nothing...");
            }
        }
    }

    public static boolean attractionCheck(String hewan) {
        String input;

        ArrayList<Attractions> attractions = Attractions.findAttractions(hewan);

        boolean isEmpty = true;

        for (Attractions attraction : attractions) {
            if (!attraction.getPerformers().isEmpty()) {
                isEmpty = false;
                break;
            }
        }

        if (isEmpty) {
            System.out.println("\nUnfortunately, no " + hewan + " can perform any attraction, please choose other animals");
            return true;
        }

        while (true) {
            System.out.println("\n--" + hewan + "--");
            int i = 0;
            ArrayList<String> temp = new ArrayList<>();
            for (SelectedAttraction attraction : attractions) {
                System.out.println(++i + ". " + attraction.getName());
                temp.add(attraction.getName());
            }

            System.out.print("Please choose your preferred attractions (type the number): ");

            input = inp.nextLine();
            if (input.equals("#")) return true;

            try {
                int inp = Integer.parseInt(input);
                if (inp > 0 && inp <= i)
                    if (!lastCheck(attractions.get(inp - 1)))
                        return false;
                    else
                        System.out.println("You do nothing...");
            } catch (NumberFormatException e) {
                System.out.println("You do nothing...");
            }
        }
    }

    public static boolean lastCheck(SelectedAttraction atraksi) {

        System.out.println("\nWow, one more step,");
        System.out.print("please let us know your name: ");
        String name = inp.nextLine();

        Registration visitor = Visitors.findVisitor(name);

        if (visitor == null) {
            visitor = new Visitors(name);
        }

        System.out.println("\nYeay, final check!");
        System.out.println("Here is your data, and the attraction you chose:");
        System.out.println("Name: " + visitor.getVisitorName());
        System.out.println("Attractions: " + atraksi.getName() + " -> " + atraksi.getType());
        System.out.print("With:");

        String output = "";

        for (Animal animal : atraksi.getPerformers()) {
            output += " " + animal.getName() + ",";
        }

        System.out.println(output.substring(0, output.length() - 1));

        System.out.print("\nIs the data correct? (Y/N): ");

        if (inp.nextLine().equalsIgnoreCase("Y")) {
            visitor.addSelectedAttraction(atraksi);

            System.out.print("\nThank you for your interest. Would you like to register to other attractions? (Y/N): ");

            if (inp.nextLine().equalsIgnoreCase("Y")) {
                return true;
            }
            else {

                String textPath = System.getProperty("user.dir") + "\\src\\main\\java";
                for (Registration visitorRn : Visitors.getVisitors()) {
                    try {
                        RegistrationWriter.writeJson(visitorRn, Paths.get(textPath));
                    } catch (IOException e) {
                        System.out.println("Something went wrong with " + visitorRn.getVisitorName());
                    }
                }
                return false;
            }
        } else
            return true;

    }
}