package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;

public class AnimalCategories extends CsvReader {

    private static int temp = 0;
    private static Map<String, ArrayList<String>> section = new LinkedHashMap<>();

    public AnimalCategories(Path file) throws IOException {
        super(file);
    }

    public static Map<String, ArrayList<String>> getSection() {
        return section;
    }

    public long countValidRecords() {
        Set<String> inpFile = new LinkedHashSet<>();

        for (String baris : lines) {
            String[] infoInput = baris.split(",");
            boolean input = false;
            if (validCategory.containsKey(infoInput[1]))
                input = validCategory.get(infoInput[1]).contains(infoInput[0]);
            boolean input2 = false;
            if(animalMap.containsKey(infoInput[2]))
                input2 = animalMap.get(infoInput[2]).contains(infoInput[0]);
            input &= ((temp != 0) || input2);
            if (input) {
                if((temp == 0)){
                    inpFile.add(infoInput[2]);
                    if(section.containsKey(infoInput[2]))
                        section.get(infoInput[2]).add(infoInput[0]);
                    else
                        section.put(infoInput[2], new ArrayList<>(Arrays.asList(infoInput[0])));
                }
                else{
                    inpFile.add(infoInput[1]);
                }
            }
        }
        return (long) inpFile.size();
    }

    public long countInvalidRecords() {
        long checkInvalid = 0;

        for (String baris : lines) {
            String[] info = baris.split(",");
            boolean input = false;
            if (validCategory.containsKey(info[1]))
                input = validCategory.get(info[1]).contains(info[0]);
            boolean input2 = false;
            if(animalMap.containsKey(info[2]))
                input2 = animalMap.get(info[2]).contains(info[0]);
            input &= ((temp != 0) || input2);
            if (!input) {
                checkInvalid++;
            }
        }
        temp++;
        return checkInvalid;
    }
}
