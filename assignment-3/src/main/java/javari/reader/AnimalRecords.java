package javari.reader;

import javari.animal.Animal;
import javari.animal.Aves;
import javari.animal.Mamals;
import javari.animal.Reptiles;
import javari.park.Attractions;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

public class AnimalRecords extends CsvReader {

    private static long valid, total;

    public AnimalRecords(Path file) throws IOException {
        super(file);
    }

    public long countValidRecords() {
        valid = 0;
        total = 0;

        for (String line : lines) {
            String[] infoInput = line.split(",");
            Animal temp;
            total++;

            if (validCategory.get("aves").contains(infoInput[1])) temp = new Aves(infoInput);
            else if (validCategory.get("mammals").contains(infoInput[1])) temp = new Mamals(infoInput);
            else if (validCategory.get("reptiles").contains(infoInput[1])) temp = new Reptiles(infoInput);
            else continue;

            registerAnimal(temp, infoInput);
            valid++;
        }
        return valid;
    }

    public long countInvalidRecords() {
        return total - valid;
    }

    public void registerAnimal(Animal animal, String[] info) {
        for (Map.Entry<String, List<String>> entry : attractionsMap.entrySet()) {
            if (entry.getValue().contains(info[1])) {
                Attractions attraction = Attractions.findAttraction(entry.getKey(), info[1]);
                if (attraction == null) attraction = new Attractions(entry.getKey(), info[1]);
                attraction.addPerformer(animal);
            }
        }
    }
}
