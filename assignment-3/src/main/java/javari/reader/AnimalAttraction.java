package javari.reader;

import javari.park.Attractions;

import java.io.IOException;
import java.nio.file.Path;
import java.util.LinkedHashSet;
import java.util.Set;

public class AnimalAttraction extends CsvReader {

    public AnimalAttraction(Path file) throws IOException {
        super(file);
    }

    public long countValidRecords() {
        Set<String> inpFile = new LinkedHashSet<>();

        for (String baris : lines) {
            String[] info = baris.split(",");
            boolean input;
            if (( ( attractionsMap.containsKey(info[1]) ) ?
                    attractionsMap.get(info[1]).contains(info[0]) : false )) input = true;
            else input = false;
            if (input) {
                inpFile.add(info[1]);
                new Attractions(info[1], info[0]);
            }
        }
        return (long) inpFile.size();
    }

    public long countInvalidRecords() {
        long checkInvalid = 0;

        for (String baris : lines) {
            String[] info = baris.split(",");
            boolean input;
            if (( ( attractionsMap.containsKey(info[1]) ) ?
                    attractionsMap.get(info[1]).contains(info[0]) : false )) input = true;
            else input = false;
            if (!input)
                checkInvalid++;
        }
        return checkInvalid;
    }
}
