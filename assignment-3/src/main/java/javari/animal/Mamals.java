package javari.animal;

public class Mamals extends Animal {

    protected String sikon;

    public Mamals(String[] infoInput) {
        super(Integer.parseInt(infoInput[0]), infoInput[1], infoInput[2], Gender.parseGender(infoInput[3]), Double.parseDouble(infoInput[4]),
                Double.parseDouble(infoInput[5]), Condition.parseCondition(infoInput[7]));
        sikon = infoInput[6];
    }

    protected boolean specificCondition() {
        return !sikon.equals("pregnant") && ((!getType().equals("Lion")) || (getGender().equals(Gender.MALE)));
    }
}
