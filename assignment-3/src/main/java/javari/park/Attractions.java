package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;
import java.util.List;

public class Attractions implements SelectedAttraction {

    private static ArrayList<Attractions> listOfAttractions = new ArrayList<>();

    private String name;
    private String type;
    private List<Animal> animalPerforms = new ArrayList<>();

    public Attractions() {}

    public Attractions(String name, String type) {
        this.name = name;
        this.type = type;
        listOfAttractions.add(this);
    }

    public String getName() {return name;}
    public String getType() {return type;}
    public List<Animal> getPerformers() {return animalPerforms;}
    public ArrayList<Attractions> getAttractionsList() {return listOfAttractions;}

    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            animalPerforms.add(performer);
            return true;
        }
        return false;
    }

    public static Attractions findAttraction(String name, String type) {
        for (Attractions atraksi : listOfAttractions) {
            if (atraksi.getName().equals(name) && atraksi.getType().equals(type))
                return atraksi;
        }
        return null;
    }

    public static ArrayList<Attractions> findAttractions(String type) {
        ArrayList<Attractions> attractionList = new ArrayList<>();
        for (Attractions atraksi : listOfAttractions) {
            if (atraksi.getType().equals(type))
                attractionList.add(atraksi);
        }
        return attractionList;
    }
}
