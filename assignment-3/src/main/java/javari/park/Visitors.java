package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitors implements Registration {

    static ArrayList<Registration> visitor = new ArrayList<>();
    static int countId = 0;

    String name;
    int idRegist;
    List<SelectedAttraction> attractions = new ArrayList<>();

    public Visitors(String name) {
        this.name = name;
        idRegist = ++countId;
        visitor.add(this);
    }

    public static ArrayList<Registration> getVisitors() {return visitor;}
    public String setVisitorName(String name) {
        this.name = name;
        return name;
    }
    public String getVisitorName() {return name;}
    public int getRegistrationId() {return idRegist;}
    public List<SelectedAttraction> getSelectedAttractions() {return attractions;}

    public static Registration findVisitor(String name) {
        for (Registration x : visitor) {
            if (x.getVisitorName().equalsIgnoreCase(name))
                return x;
        }
        return null;
    }

    public boolean addSelectedAttraction(SelectedAttraction selected) {
        attractions.add(selected);
        return true;
    }
}
