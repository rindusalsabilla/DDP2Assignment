import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms
    
    // You can add new variables or methods in this class

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        WildCat kucing;
        TrainCar kereta = null;
        int counter = 0;

        int inpNum = Integer.parseInt(input.nextLine());
        for (int i = 0; i < inpNum; i++){
        	String inpCat = input.nextLine();
        	String inpSplit[] = inpCat.split(",");
        	String catName = inpSplit[0];
        	double catWeight = Double.parseDouble(inpSplit[1]);
        	double catLength = Double.parseDouble(inpSplit[2]);
        	kucing = new WildCat(catName, catWeight, catLength);

        	if(counter == 0)
        		kereta = new TrainCar(kucing);
        	else
        		kereta = new TrainCar(kucing, kereta);

        	counter++;

        	if(kereta.computeTotalWeight() > THRESHOLD || i==inpNum-1){
	        	System.out.println("The train departs to Javari Park");
	        	System.out.print("[LOCO]<--");
	        	kereta.printCar();
	        	System.out.println("");
	        	double average = kereta.computeTotalMassIndex()/counter;
	        	System.out.printf("Average mass index of all cats: %.2f%n", average);
	        	if (average < 18.5) System.out.println("In average, the cats in the train are *underweight*");
	        	else if (average >= 18.5 && average < 25) System.out.println("In average, the cats in the train are *normal*");
	        	else if (average >= 25 && average < 30) System.out.println("In average, the cats in the train are *overweight*");
	        	else System.out.println("In average, the cats in the train are *obese*");
        		counter = 0;
	        }
        }







    }
}
