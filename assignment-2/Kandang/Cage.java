package Kandang;
import Animal.Animals;


public class Cage{
	private Animals animal;
	private String type;
	private String location;

	public Cage(Animals animal, String location){
		this.animal = animal;
		this.location = location;

		if (location.equals("indoor")){
			if (animal.getLength() < 45) this.type = "A";
			else if (animal.getLength() > 45 && animal.getLength() < 60) this.type = "B";
			else this.type = "C";
		}
		else if (location.equals("outdoor")){
			if (animal.getLength() < 75) this.type = "A";
			else if (animal.getLength() > 75 && animal.getLength() < 90) this.type = "B";
			else this.type = "C";
		}
	}

	public String getType(){return type;}
	public Animals getAnimal(){return animal;}
}