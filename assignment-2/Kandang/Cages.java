package Kandang;
import java.util.ArrayList;
import Animal.Animals;
import Kandang.Cage;

public class Cages{
	//membuat kandang besar yang berisi arraylist dari kandang-kandang
	public static ArrayList<ArrayList<Cage>> catCage = new ArrayList<ArrayList<Cage>>();
	public static ArrayList<ArrayList<Cage>> hamsterCage = new ArrayList<ArrayList<Cage>>();
	public static ArrayList<ArrayList<Cage>> parrotCage = new ArrayList<ArrayList<Cage>>();
	public static ArrayList<ArrayList<Cage>> lionCage = new ArrayList<ArrayList<Cage>>();
	public static ArrayList<ArrayList<Cage>> eagleCage = new ArrayList<ArrayList<Cage>>();

	//Membuat kandang dan memasukkan kedalam level
	public static void createCage(ArrayList<Animals> listofAnimal, String species){
		ArrayList<ArrayList<Cage>> cageList = null;
		int levelOfAnimal = listofAnimal.size()/3;
		if (listofAnimal.size() < 3) levelOfAnimal = 1;
		String location= "";

		if (species == "cat"){
			cageList = catCage;
			location = "indoor";
		}
		else if (species == "lion"){
			cageList = lionCage;
			location = "outdoor";
		}
		else if (species == "hamster"){
			cageList = hamsterCage;
			location = "indoor";
		}
		else if (species == "parrot"){
			cageList = parrotCage;
			location = "indoor";
		}
		else if (species == "eagle"){
			cageList = eagleCage;
			location = "outdoor";
		}

		for (int i = 0; i<3; i++){
			cageList.add(new ArrayList<Cage>());
		}

		int level = 0;
		int counter = 1;

		for (Animals animal : listofAnimal){
			if (level == 2){
				Cage cageAnimal = new Cage(animal, location);
				ArrayList<Cage> cageLevel = cageList.get(level);
				cageLevel.add(cageAnimal);
				continue;
			}

			if (counter > levelOfAnimal) {
				level++;
				counter = 1;
			}

			Cage cageAnimal = new Cage(animal, location);
			ArrayList<Cage> cageLevel = cageList.get(level);
			cageLevel.add(cageAnimal);
			counter++;
		}	
	}

	public static void printCage(String species){
		ArrayList<ArrayList<Cage>> cageList = null;
		String location= "";

		if (species == "cat"){
			cageList = catCage;
			location = "indoor";
		}
		else if (species == "lion"){
			cageList = lionCage;
			location = "outdoor";
		}
		else if (species == "hamster"){
			cageList = hamsterCage;
			location = "indoor";
		}
		else if (species == "parrot"){
			cageList = parrotCage;
			location = "indoor";
		}
		else if (species == "eagle"){
			cageList = eagleCage;
			location = "outdoor";
		}
		//mencetak nama hewan dan level
		System.out.println("location: " + location);
		for (int i = 2; i >= 0; --i){
			System.out.print("level " + (i+1) + ": ");
			for (int j = 0; j<cageList.get(i).size(); j++ ) {
				System.out.print(cageList.get(i).get(j).getAnimal().getAnimalName() + " (" + cageList.get(i).get(j).getAnimal().getLength() + " - " + cageList.get(i).get(j).getType() + "), ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println();

		System.out.println("After rearragement...");

		//Mengubah level
		for (int i = 0; i<3; i++){
			ArrayList<Cage> temp = cageList.get(i);
			for(int j = 0; j < cageList.get(i).size(); j++){
				temp.add(j, temp.remove(temp.size()-1));
			}
		}
		cageList.add(0,cageList.remove(cageList.size()-1));
		
		//Mengubah posisi nama tiap level
		for (int i = 2; i >= 0; --i){
			System.out.print("level " + (i+1) + ": ");
			for (int j = 0; j<cageList.get(i).size(); j++ ) {
				System.out.print(cageList.get(i).get(j).getAnimal().getAnimalName() + " (" + cageList.get(i).get(j).getAnimal().getLength() + " - " + cageList.get(i).get(j).getType() + "), ");
			}
			System.out.println();
		}
		System.out.println();
		System.out.println();
	}
}