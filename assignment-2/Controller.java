import java.util.Scanner;
import java.util.ArrayList;
import java.util.List;
import Animal.*;
import Kandang.*;

public class Controller{

	public static void main(String[] args) {
		//Membuat arraylist dari masing-masing hewan
		ArrayList<Animals> cat = new ArrayList<Animals>();
		ArrayList<Animals> hamster = new ArrayList<Animals>();
		ArrayList<Animals> lion = new ArrayList<Animals>();
		ArrayList<Animals> parrot = new ArrayList<Animals>();
		ArrayList<Animals> eagle = new ArrayList<Animals>();

		Scanner input = new Scanner(System.in);

		System.out.println("Welcome to Javari Park!");
		System.out.println("Input the number of animals");
		String[] listOfAnimal = {"cat", "lion", "eagle", "parrot", "hamster"};
		//Mencetak input untuk memasukkan data hewan
		for (int j =0; j<listOfAnimal.length; j++){
			System.out.print(listOfAnimal[j] + ": ");
			int numAnimal = input.nextInt();
			if (numAnimal > 0){
				System.out.println("Provide the information of " + listOfAnimal[j] + "(s):");
				String[] listInfoAnimal = input.next().split(",");
				
				if (listOfAnimal[j].equals("cat")){
					for (int i=0; i<listInfoAnimal.length; i++){
						String[] infoAnimal = listInfoAnimal[i].split("\\|");
						Cat kucing = new Cat(infoAnimal[0], Integer.parseInt(infoAnimal[1]));
						cat.add(kucing);
					}
				}
				if (listOfAnimal[j].equals("lion")){
					for (int i=0; i<listInfoAnimal.length; i++){
						String[] infoAnimal = listInfoAnimal[i].split("\\|");
						Lion singa = new Lion(infoAnimal[0], Integer.parseInt(infoAnimal[1]));
						lion.add(singa);
					}
				}
				if (listOfAnimal[j].equals("hamster")){
					for (int i=0; i<listInfoAnimal.length; i++){
						String[] infoAnimal = listInfoAnimal[i].split("\\|");
						Hamster ham = new Hamster(infoAnimal[0], Integer.parseInt(infoAnimal[1]));
						hamster.add(ham);
					}
				}
				if (listOfAnimal[j].equals("parrot")){
					for (int i=0; i<listInfoAnimal.length; i++){
						String[] infoAnimal = listInfoAnimal[i].split("\\|");
						Parrot burung = new Parrot(infoAnimal[0], Integer.parseInt(infoAnimal[1]));
						parrot.add(burung);
					}
				}
				if (listOfAnimal[j].equals("eagle")){
					for (int i=0; i<listInfoAnimal.length; i++){
						String[] infoAnimal = listInfoAnimal[i].split("\\|");
						Eagle elang = new Eagle(infoAnimal[0], Integer.parseInt(infoAnimal[1]));
						eagle.add(elang);
					}
				}
			}
		}
		System.out.println("Animals have been successfully recorded!");
		System.out.println();
		System.out.println();
		System.out.println("=============================================");
		System.out.println("Cage arrangement:");
		//Memasukkan hewan kedalam level-level
		if (cat.size() > 0){
			Cages.createCage(cat, "cat");
			Cages.printCage("cat");
		}	
		if (lion.size() > 0){
			Cages.createCage(lion, "lion");
			Cages.printCage("lion");
		}
		if (hamster.size() > 0){
			Cages.createCage(hamster, "hamster");
			Cages.printCage("hamster");
		}	
		if (parrot.size() > 0){
			Cages.createCage(parrot, "parrot");
			Cages.printCage("parrot");
		}
		if (eagle.size() > 0){
			Cages.createCage(eagle, "eagle");
			Cages.printCage("eagle");
		}

		System.out.println("NUMBER OF ANIMALS:");
		System.out.println("cat:" + cat.size());
		System.out.println("lion:" + lion.size());
		System.out.println("parrot:" + parrot.size());
		System.out.println("eagle:" + eagle.size());
		System.out.println("hamster:" + hamster.size());
		System.out.println();
		System.out.println();

		System.out.println("=============================================");
		//Melakukan method hewan
		while(true){
			System.out.println("Which animal you want to visit?");
			System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99:exit)");
			int animal = input.nextInt();

			String name = "";
			ArrayList<Animals> anim = null;
			String[] names = {"cat", "eagle"};
			System.out.print("Mention the name of " + names[animal-1] + " you want to visit: ");
			input.nextLine();
			name = input.nextLine();
			if (animal == 1) {
				anim = cat;
			} else if (animal == 2) {
				anim = eagle;
			}

			Animals hewan = checkAnimal(name, anim);

			if (hewan != null) {
				hewan.action();
			}
			else {
				System.out.println("There is no " + names[animal-1] + " with that name! Back to the office!\n");
			}



			if (animal == 1){
				System.out.print("Mention the name of cat you want to visit: ");
				input.nextLine();
				String name = input.nextLine();
				for (int i = 0; i < cat.size(); i++){
					if (name.equals(cat.get(i).getAnimalName())) {
						System.out.println("You are visiting " + name + " (cat) now, what would you like to do?");
						((Cat) cat.get(i)).action();
						System.out.println("Back to the office!\n");
					}
				}
				if (checkAnimal(name, cat) == false) System.out.println("There is no cat with that name! Back to the office!\n");
			}
			if (animal == 2){
				System.out.print("Mention the name of eagle you want to visit: ");
				input.nextLine();
				String name = input.nextLine();
				for (int i = 0; i < eagle.size(); i++){
					if (name.equals(eagle.get(i).getAnimalName())) {
						System.out.println("You are visiting " + name + " (eagle) now, what would you like to do?");
						((Eagle) eagle.get(i)).action();
						System.out.println("Back to the office!\n");
					}
				}
				if (checkAnimal(name, eagle) == false) System.out.println("There is no eagle with that name! Back to the office!\n");
			}
			if (animal == 3){
				System.out.print("Mention the name of hamster you want to visit: ");
				input.nextLine();
				String name = input.nextLine();
				for (int i = 0; i < hamster.size(); i++){
					if (name.equals(hamster.get(i).getAnimalName())) {
						System.out.println("You are visiting " + name + " (hamster) now, what would you like to do?");
						((Hamster) hamster.get(i)).action();
						System.out.println("Back to the office!\n");

					}
				}
				if (checkAnimal(name, hamster) == false) System.out.println("There is no hamster with that name! Back to the office!\n");
			}
			if (animal == 4){
				System.out.print("Mention the name of parrot you want to visit: ");
				input.nextLine();
				String name = input.nextLine();
				for (int i = 0; i < parrot.size(); i++){
					if (name.equals(parrot.get(i).getAnimalName())) {
						System.out.println("You are visiting " + name + " (parrot) now, what would you like to do?");
						((Parrot) parrot.get(i)).action();
						System.out.println("Back to the office!\n");
					}
				}
				if (checkAnimal(name, parrot) == false) System.out.println("There is no parrot with that name! Back to the office!\n");
			}
			if (animal == 5){
				System.out.print("Mention the name of lion you want to visit: ");
				input.nextLine();
				String name = input.nextLine();
				for (int i = 0; i < lion.size(); i++){
					if (name.equals(lion.get(i).getAnimalName())) {
						System.out.println("You are visiting " + name + " (lion) now, what would you like to do?");
						((Lion) lion.get(i)).action();
						System.out.println("Back to the office!\n");

					}
				}
				if (checkAnimal(name, lion) == false) System.out.println("There is no lion with that name! Back to the office!\n");
			}

			if (animal == 99) break;

		}
		
	}	
// Untuk mengecek ada atau tidak hewan didalam list hewan
	public static Animals checkAnimal(String name, ArrayList<Animals> listHewan){
		for (int i = 0; i < listHewan.size(); i++){
			if(name.equals(listHewan.get(i).getAnimalName())) return listHewan.get(i);
		}
		return null;
	}
}