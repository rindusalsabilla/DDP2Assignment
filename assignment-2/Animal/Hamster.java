package Animal;
import java.util.Scanner;

public class Hamster extends Animals{
	public Hamster(String name, int length){
		super(name, length, "hamster");
	}

	public void action(){
		Scanner input = new Scanner(System.in);
		System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
		String actNum = input.nextLine();
		if (actNum.equals("1")){
			System.out.println(name + " makes a voice: ngkkrit.. ngkkrrriiit");
		}
		else if (actNum.equals("2")){
			System.out.println(name + " makes a voice: trrr…. trrr...");
		}
		else{
			System.out.println("You do nothing...");
		}
	}
}