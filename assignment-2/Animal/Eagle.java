package Animal;
import java.util.Scanner;

public class Eagle extends Animals{
	public Eagle(String name, int length){
		super(name, length, "eagle");
	}

	public void action(){
		Scanner input = new Scanner(System.in);
		System.out.println("1: Order to fly");
		String actNum = input.nextLine();
		if(actNum.equals("1")){
			System.out.println(name + " makes a voice: kwaakk…");
			System.out.println("You hurt!");
		}
		else{
			System.out.println("You do nothing...");
		}
	}
}