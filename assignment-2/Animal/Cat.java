package Animal;
import java.util.*;

public class Cat extends Animals{
	public Cat(String name, int length){
		super(name, length, "cat");


	}

	public void action(){
		Random random = new Random();
		Scanner input = new Scanner(System.in);
		String[] sound = {"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
		System.out.println("1: Brush the fur 2: Cuddle");
		String actNum = input.nextLine();
		if (actNum.equals("1")) {
			System.out.println("Time to clean " + name + "'s fur");
			System.out.println(name + " makes a voice: Nyaaan...");
		}
		else if (actNum.equals("2")) {
			int n = random.nextInt(4);
			System.out.println(name + " makes a voice: " + sound[n]);
		}
		else {
			System.out.println("You do nothing...");
		}

	}

}