package Animal;
import java.util.Scanner;

public class Parrot extends Animals{
	public Parrot(String name, int length){
		super(name, length, "parrot");
	}

	public void action(){
		Scanner input = new Scanner(System.in);
		System.out.println("1: Order to fly 2: Do conversation");
		String actNum = input.nextLine();
		if (actNum.equals("1")){
			System.out.println("Parrot " + name + " flies!");
			System.out.println(name + " makes a voice: FLYYYY…..");
		}
		else if (actNum.equals("2")){
			System.out.print("You say: ");
			String say = input.nextLine();
			System.out.println(name + " says: " + say.toUpperCase());
		}
		else{
			System.out.println(name + " says: HM?");
		}
	}
}