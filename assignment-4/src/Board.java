import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import javax.swing.*;


public class Board extends JFrame {
    private ArrayList<Card> bttn;
    private Card c1=null, c2=null;
    private int counter = 0;
    private JLabel label = new JLabel("Number of tries: " + counter);

    // Constructor to setup GUI components and event handlers
    public Board () {

        // Set up display panel
        Panel panelDisplay = new Panel();

        //Membuat button Play Again dan memasukkan action listener
        JButton button = new JButton("Play Again?");
        panelDisplay.add(button);
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                restart();
            }
        });

        //Membuat button exit dan memasukkan action listener
        JButton button2 = new JButton("Exit");
        panelDisplay.add(button2);
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        // Membuat grid layout pada button
        JPanel panelButtons = new JPanel(new GridLayout(6, 6));
        bttn = new ArrayList<>();
        Card c;
        for (int i = 0; i < 18; i++){
            // dua kali karena fotonya mau didobel
            c = new Card(i+1);
            c.setIcon(Card.imageIcon);
            bttn.add(c);
            c = new Card(i+1);
            c.setIcon(Card.imageIcon);
            bttn.add(c);
        }

        Collections.shuffle(bttn);
        for(Card card : bttn){
            panelButtons.add(card);
        }
        setButtonAction();

        // "super" Frame sets to BorderLayout
        setLayout(new BorderLayout());
        add(panelDisplay, BorderLayout.CENTER);
        add(panelButtons, BorderLayout.PAGE_START);
        add(label, BorderLayout.PAGE_END);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setTitle("Remember their twin"); // "super" Frame sets title
        setSize(1500, 1550); // "super" Frame sets initial size
        setVisible(true); // "super" Frame shows
    }

    /**
     * Method ini untuk merestart game
     */
    public void restart(){
        c1 = null;
        c2 = null;
        this.dispose();
        new Board();
    }

    /**
     * method action listener untuk menangani perubahan gambar ketika di klik
     */
    public void setButtonAction(){
        for(Card card : bttn){
            card.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    turnCard((Card) e.getSource());
                }
            });
        }
    }

    /**
     * method ini untuk mengubah gambar yang di klik menjadi gambar yang di ingat
     * @param c
     */
    public void turnCard(Card c){

        if(c1 == null && c2 == null){
            c1 = c;
            c1.setIcon(c1.getPic());
        }

        if (c1 != null && c2 == null && c1!= c) {
            c2 = c;
            c2.setIcon(c2.getPic());

            Timer t = new Timer(750, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    checkCard();
                }
            });

            t.setRepeats(false);
            t.start();
        }
    }

    /**
     * method untuk mengecek apakah gambar sama atau tidak
     */
    public void checkCard(){
        if(c1.getId() == c2.getId()){
            c1.setMatch(true);
            c2.setMatch(true);
            c1.setEnabled(false);
            c2.setEnabled(false);

            if (isWin()){
                JOptionPane.showMessageDialog(this, "Congratulation you WIN!");
            }
        }

        //apabila gambar tidak sama maka akan diubah kegambar default semula
        else{
            c1.setIcon(Card.imageIcon);
            c2.setIcon(Card.imageIcon);
        }

        c1 = null;
        c2 = null;
        counter++;
        label.setText("Number of tries: " + counter);
    }

    /**
     * method apabila sudah menang
     * @return true
     */
    public boolean isWin(){
        for (Card card : bttn){
            if (card.isMatch() == false) return false;
        }
        return true;
    }
}