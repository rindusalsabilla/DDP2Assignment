import javax.swing.*;
import java.awt.*;

public class Card extends JButton {
    public static final ImageIcon imageIcon = resolution(new ImageIcon("C:\\Users\\ASUS\\Documents\\Materi 1\\SEMESTER 2\\DDP2\\DDP2Assignment\\assignment-4\\tgs\\0.jpg"));
    private int id;
    private ImageIcon pic;
    private boolean match = false;

    public Card(int id){
        this.id = id;
        this.pic = resolution(new ImageIcon("C:\\Users\\ASUS\\Documents\\Materi 1\\SEMESTER 2\\DDP2\\DDP2Assignment\\assignment-4\\tgs\\" + id + ".jpg"));
    }

    public ImageIcon getPic() {
        return pic;
    }

    public int getId(){return this.id;}
    public void setMatch(boolean match){this.match = match;}
    public boolean isMatch(){return this.match;}

    /**
     * method untuk mengganti agar ukuran gambar bisa disamaratakan
     * @param r
     * @return
     */
    private static ImageIcon resolution(ImageIcon r){
        return new ImageIcon(r.getImage().getScaledInstance(225, 225, Image.SCALE_DEFAULT));
    }
}
